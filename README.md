# DynamoDB Expression - [![npm-version](https://img.shields.io/npm/v/@typemon/dynamodb-expression.svg)](https://www.npmjs.com/package/@typemon/dynamodb-expression) [![npm-downloads](https://img.shields.io/npm/dt/@typemon/dynamodb-expression.svg)](https://www.npmjs.com/package/@typemon/dynamodb-expression)
> DynamoDB expression builder for TypeScript



# Features
- Support almost all expressions
- Automatic key mapping and anti-collision

+ [Condition Expression](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ConditionExpressions.html)
    + [Comparison](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Comparators)
        + equal
        + not equal
        + less than
        + less than or equal
        + greater than
        + greater than or equal
        + between
        + in
    + [Function](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions)
        + attribute exists
        + attribute not exists
        + attribute type
        + begins with
        + contains
        + size
    + [Logical](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.LogicalEvaluations)
        + not
        + and
        + or
        + parentheses
+ [Update Expression](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html)
    + [set](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET)
        + [increment](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.IncrementAndDecrement)
        + [decrement](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.IncrementAndDecrement)
        + [list append](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.UpdatingListElements)
        + [if not exists](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.PreventingAttributeOverwrites)
    + [remove](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.REMOVE)



## Installation
```
$ npm install @typemon/dynamodb-expression
```
```typescript
import {
    /* Expression Classes */
    Expression,
    UpdateExpression,

    /* Logical Expressions */
    not,
    and,
    or,
    parentheses,

    /* Comparison Expressions */
    equal,
    notEqual,
    lessThan,
    lessThanOrEqual,
    greaterThan,
    greaterThanOrEqual,

    betweenAnd,
    inList,

    /* Function Expressions */
    attributeExists,
    attributeNotExists,
    attributeType,
    beginsWith,
    contains,
    size,

    /* Update Expressions */
    set,
    remove,
    increment,
    decrement,
    listAppend,
    ifNotExists,

    /* Alias Helper */
    Alias,

    /* Types */
    Value,
    Binary,
    Set

    /* Expressions Classes */
    . . .
} from '@typemon/dynamodb-expression';
```



## Usage
### Condition
```typescript
const client: DocumentClient = new DocumentClient();
const keyConditionExpression: Expression = equal('partition', 'monster');
const filterExpression: Expression = and(
    equal('name', 'typemon'),
    contains('packages', 'dynamodb-expression')
);

const result: DocumentClient.QueryOutput = await client.query({
    TableName: 'monster-space-network',
    KeyConditionExpression: keyConditionExpression.expression,
    FilterExpression: filterExpression.expression,
    ExpressionAttributeNames: Object.assign(keyConditionExpression.names, filterExpression.names),
    ExpressionAttributeValues: Object.assign(keyConditionExpression.values, filterExpression.values)
}).promise();
```

### Update
```typescript
const client: DocumentClient = new DocumentClient();
const primaryKey: object = { name: 'typemon' };
const updateExpression: Expression = UpdateExpression.build([
    set('foo', 'bar'),
    increment('count', 1),
    remove('temp')
]);

const result: DocumentClient.UpdateItemOutput = await client.update({
    TableName: 'monster-space-network',
    Key: primaryKey,
    UpdateExpression: updateExpression.expression,
    ExpressionAttributeNames: updateExpression.names,
    ExpressionAttributeValues: updateExpression.values
}).promise();
```

### Expression Example
#### Condition
```typescript
and(
    equal('foo', 'bar'),
    greaterThanOrEqual(size('items'), 100)
);
```
```typescript
{
    expression: '#dTwHE = :rYXbz AND size (#Pgcou) >= :TjhDs',
    names: {
        '#dTwHE': 'foo',
        '#Pgcou': 'items'
    },
    values: {
        ':rYXbz': 'bar',
        ':TjhDs': 100
    }
}
```

#### Update
```typescript
UpdateExpression.build([
    set('foo.0.1.2', 'bar'),
    remove('temp[0]'),
    increment('count', 1)
]);
```
```typescript
{
    expression: 'SET #YarFm[0][1][2] = :vhBOn, #sQYcx = #sQYcx + :nBhjy REMOVE #rkvSt[0]',
    names: {
        '#YarFm': 'foo',
        '#sQYcx': 'count',
        '#rkvSt': 'temp'
    },
    values: {
        ':vhBOn': 'bar',
        ':nBhjy': 1
    }
}
```
