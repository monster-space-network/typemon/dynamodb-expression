import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
//
//
//
/**
 * @example
 * #path = #path + :value
 */
export class IncrementExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression,
        private readonly value: number
    ) {
        super(Clause.Set);
    }

    protected express(): Expression {
        const pathExpression: Expression = Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
        const valueAlias: string = Alias.generateValueAlias();

        return new Expression({
            expression: `${pathExpression.expression} = ${pathExpression.expression} + ${valueAlias}`,
            names: pathExpression.names,
            values: {
                [valueAlias]: this.value
            }
        });
    }
}

/**
 * @example
 * #path = #path + :value
 */
export const increment: (path: string | Expression, value: number) => IncrementExpression
    = (path: string | Expression, value: number): IncrementExpression => new IncrementExpression(path, value);
