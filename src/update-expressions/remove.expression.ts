import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
//
//
//
/**
 * @example
 * #path
 */
export class RemoveExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression
    ) {
        super(Clause.Remove);
    }

    protected express(): Expression {
        return Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
    }
}

/**
 * @example
 * #path
 */
export const remove: (path: string | Expression) => RemoveExpression
    = (path: string | Expression): RemoveExpression => new RemoveExpression(path);
