import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
import { Value } from '../types';
//
//
//
/**
 * @example
 * #path = if_not_exists (#path, :value)
 */
export class IfNotExistsExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression,
        private readonly value: Value
    ) {
        super(Clause.Set);
    }

    protected express(): Expression {
        const pathExpression: Expression = Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
        const valueAlias: string = Alias.generateValueAlias();

        return new Expression({
            expression: `${pathExpression.expression} = if_not_exists (${pathExpression.expression}, ${valueAlias})`,
            names: pathExpression.names,
            values: {
                [valueAlias]: this.value
            }
        });
    }
}

/**
 * @example
 * #path = if_not_exists (#path, :value)
 */
export const ifNotExists: (path: string | Expression, value: Value) => IfNotExistsExpression
    = (path: string | Expression, value: Value): IfNotExistsExpression => new IfNotExistsExpression(path, value);
