import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
import { Value } from '../types';
//
//
//
/**
 * @example
 * #path = :value
 */
export class SetExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression,
        private readonly value: Value
    ) {
        super(Clause.Set);
    }

    protected express(): Expression {
        const pathExpression: Expression = Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
        const valueAlias: string = Alias.generateValueAlias();

        return new Expression({
            expression: `${pathExpression.expression} = ${valueAlias}`,
            names: pathExpression.names,
            values: {
                [valueAlias]: this.value
            }
        });
    }
}

/**
 * @example
 * #path = :value
 */
export const set: (path: string | Expression, value: Value) => SetExpression
    = (path: string | Expression, value: Value): SetExpression => new SetExpression(path, value);
