import { Expression } from '../expression';
//
//
//
export enum Clause {
    Set,
    Remove
}

type ClauseKeyword
    = 'SET'
    | 'REMOVE';

interface ClauseKeywords {
    readonly [Clause.Set]: 'SET';
    readonly [Clause.Remove]: 'REMOVE';
}

const CLAUSE_KEYWORDS: ClauseKeywords = {
    [Clause.Set]: 'SET',
    [Clause.Remove]: 'REMOVE'
};

interface Classified extends ReadonlyArray<UpdateExpression[]> {
    readonly [Clause.Set]: UpdateExpression[];
    readonly [Clause.Remove]: UpdateExpression[];
}

export abstract class UpdateExpression {
    private static merge(updateExpressions: ReadonlyArray<UpdateExpression>): Expression {
        return updateExpressions.reduce((expression: Expression, updateExpression: UpdateExpression, index: number): Expression => {
            const separator: string = index > 0 ? ', ' : '';
            const expressed: Expression = updateExpression.express();

            return new Expression({
                expression: expression.expression + separator + expressed.expression,
                names: Object.assign(expression.names, expressed.names),
                values: Object.assign(expression.values, expressed.values)
            });
        }, Expression.empty());
    }

    /**
     * @description Merges the update expressions and adds clauses. Update expressions are not immediately available. Must build.
     */
    public static build(updateExpressions: ReadonlyArray<UpdateExpression>): Expression {
        return updateExpressions
            .reduce((classified: Classified, updateExpression: UpdateExpression): Classified => {
                classified[updateExpression.clause].push(updateExpression);

                return classified;
            }, [[], [], [], []])
            .filter((updateExpressions: ReadonlyArray<UpdateExpression>): boolean => updateExpressions.length > 0)
            .reduce((expression: Expression, updateExpressions: ReadonlyArray<UpdateExpression>, index: number): Expression => {
                const clause: Clause = updateExpressions[0].clause;
                const clauseKeyword: ClauseKeyword = CLAUSE_KEYWORDS[clause];
                const separator: string = index > 0 ? ' ' : '';
                const mergedUpdateExpression: Expression = this.merge(updateExpressions);

                return new Expression({
                    expression: `${expression.expression}${separator}${clauseKeyword} ${mergedUpdateExpression.expression}`,
                    names: Object.assign(expression.names, mergedUpdateExpression.names),
                    values: Object.assign(expression.values, mergedUpdateExpression.values)
                });
            }, Expression.empty());
    }

    protected constructor(
        protected readonly clause: Clause
    ) { }

    protected abstract express(): Expression;

    /**
     * @description Add a clause to the expression. Update expressions are not immediately available. Must build.
     * @example
     * 'SET' + expression
     */
    public build(): Expression {
        const clauseKeyword: ClauseKeyword = CLAUSE_KEYWORDS[this.clause];
        const expressed: Expression = this.express();

        return new Expression({
            expression: `${clauseKeyword} ${expressed.expression}`,
            names: expressed.names,
            values: expressed.values
        });
    }
}
