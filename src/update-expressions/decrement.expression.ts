import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
//
//
//
/**
 * @example
 * #path = #path - :value
 */
export class DecrementExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression,
        private readonly value: number
    ) {
        super(Clause.Set);
    }

    protected express(): Expression {
        const pathExpression: Expression = Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
        const valueAlias: string = Alias.generateValueAlias();

        return new Expression({
            expression: `${pathExpression.expression} = ${pathExpression.expression} - ${valueAlias}`,
            names: pathExpression.names,
            values: {
                [valueAlias]: this.value
            }
        });
    }
}

/**
 * @example
 * #path = #path - :value
 */
export const decrement: (path: string | Expression, value: number) => DecrementExpression
    = (path: string | Expression, value: number): DecrementExpression => new DecrementExpression(path, value);
