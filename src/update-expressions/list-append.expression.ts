import { UpdateExpression, Clause } from './update-expression';
import { Expression } from '../expression';
import { Alias } from '../alias';
import { Value } from '../types';
//
//
//
/**
 * @example
 * #path = list_append (#path, :value)
 */
export class ListAppendExpression extends UpdateExpression {
    public constructor(
        private readonly path: string | Expression,
        private readonly value: ReadonlyArray<Value>
    ) {
        super(Clause.Set);
    }

    protected express(): Expression {
        const pathExpression: Expression = Expression.isExpression(this.path)
            ? this.path
            : Alias.parsePath(this.path);
        const valueAlias: string = Alias.generateValueAlias();

        return new Expression({
            expression: `${pathExpression.expression} = list_append (${pathExpression.expression}, ${valueAlias})`,
            names: pathExpression.names,
            values: {
                [valueAlias]: this.value
            }
        });
    }
}

/**
 * @example
 * #path = list_append (#path, :value)
 */
export const listAppend: (path: string | Expression, value: ReadonlyArray<Value>) => ListAppendExpression
    = (path: string | Expression, value: ReadonlyArray<Value>): ListAppendExpression => new ListAppendExpression(path, value);
