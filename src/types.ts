import { DocumentClient } from 'aws-sdk/clients/dynamodb';
//
//
//
export type Set = DocumentClient.DynamoDbSet;

export type Binary = DocumentClient.binaryType;

export type Value = string | number | boolean | null | Values | object | Binary | Set;

interface Values extends ReadonlyArray<Value> { }
