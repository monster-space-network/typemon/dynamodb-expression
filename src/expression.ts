import { ReadonlyIndex } from '@typemon/types';
//
//
//
export namespace Expression {
    export interface Data {
        /**
         * @description Expression.
         */
        readonly expression: string;

        /**
         * @description Index containing key and alias mapping information.
         */
        readonly names: ReadonlyIndex<string, string>;

        /**
         * @description Index containing value and alias mapping information.
         */
        readonly values: ReadonlyIndex<string, unknown>;
    }
}

export class Expression implements Expression.Data {
    /**
     * @description Verify that the value is an instance of the expression class.
     */
    public static isExpression(value: unknown): value is Expression {
        return value instanceof Expression;
    }

    /**
     * @description Returns an empty expression.
     */
    public static empty(): Expression {
        return new Expression();
    }

    /**
     * @description Expression. Can be used immediately.
     */
    public readonly expression: string;

    /**
     * @description Index containing key and alias mapping information. Can be used immediately.
     */
    public readonly names: ReadonlyIndex<string, string>;

    /**
     * @description Index containing value and alias mapping information. Can be used immediately.
     */
    public readonly values: ReadonlyIndex<string, unknown>;

    public constructor(data?: Partial<Expression.Data>) {
        if (!data) {
            data = {
                expression: '',
                names: {},
                values: {}
            };
        }

        this.expression = !!data.expression ? data.expression : '';
        this.names = !!data.names ? data.names : {};
        this.values = !!data.values ? data.values : {};
    }
}
