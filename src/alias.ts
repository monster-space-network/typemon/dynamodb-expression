import { Expression } from './expression';
//
//
//
export namespace Alias {
    const CHARACTERS: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUXYZ';
    const CHARACTERS_LENGTH: number = CHARACTERS.length;
    const DEFAULT_LENGTH: number = 5;

    function generateRandomCharacter(): string {
        const index: number = Math.floor(Math.random() * CHARACTERS_LENGTH);

        return CHARACTERS[index];
    }

    function generateId(): string {
        return new Array(DEFAULT_LENGTH).fill(null).reduce((id: string): string => id + generateRandomCharacter(), '');
    }

    const KEY_PREFIX: string = '#';
    const VALUE_PREFIX: string = ':';

    /**
     * @example
     * '#randomString'
     */
    export function generateKeyAlias(): string {
        return KEY_PREFIX + generateId();
    }

    /**
     * @example
     * ':randomString'
     */
    export function generateValueAlias(): string {
        return VALUE_PREFIX + generateId();
    }

    export interface ParseOptions {
        /**
         * @description If true, the numeric key changes to a list accessor.
         * @example
         * true  'a.0.b.1.2.3' => 'a[0].b[1][2][3]'
         * false 'a.0.b.1.2.3' => 'a.0.b.1.2.3'
         */
        readonly numberKeyToIndex: boolean;
    }

    const DEFAULT_PARSE_OPTIONS: ParseOptions = {
        numberKeyToIndex: true
    };

    const DELIMITER: string = '.';
    const ESCAPED_DELIMITER: string = '\\';
    const ESCAPED_DELIMITER_PATTERN: RegExp = /\\/g;
    const NUMBER_KEY_PATTERN: RegExp = /^\d+$/;
    const INDEX_PATTERN: RegExp = /\[([0-9]+)\]/g;
    const INDEX_KEY_PATTERN: RegExp = /^\[[0-9]+\]$/;

    /**
     * @description Separates paths and maps them to aliases.
     * @example
     * 'a.0.b.1.2.3' -> '#random[0].#string[1][2][3]'
     */
    export function parsePath(path: string, options?: Partial<ParseOptions>): Expression {
        if (!options) {
            options = DEFAULT_PARSE_OPTIONS;
        }

        const numberKeyToIndex: boolean = !!options.numberKeyToIndex;
        const characters: string[] = [];

        return path
            .split('')
            .reduceRight((keys: string[], character: string, index: number, splittedPath: ReadonlyArray<string>): string[] => {
                const lastCharacter: boolean = index === 0;
                const nextCharacter: string | undefined = splittedPath[index - 1];
                const isDelimiter: boolean = character === DELIMITER && nextCharacter !== ESCAPED_DELIMITER;
                let key: string = '';

                if (!isDelimiter) {
                    characters.push(character);
                }
                if (lastCharacter || isDelimiter) {
                    key = characters.splice(0, characters.length).reverse().join('');
                }

                if (key.length === 0) {
                    return keys;
                }

                key = key.replace(ESCAPED_DELIMITER_PATTERN, '');

                if (splittedPath.length - index - 1 > 0 && numberKeyToIndex && NUMBER_KEY_PATTERN.test(key)) {
                    key = `[${key}]`;
                }

                keys.push(key);

                return keys;
            }, [])
            .reduceRight((expression: Expression, key: string, index: number, keys: ReadonlyArray<string>): Expression => {
                const firstKey: boolean = index === keys.length - 1;

                if (!firstKey && INDEX_KEY_PATTERN.test(key)) {
                    return new Expression({
                        expression: expression.expression + key,
                        names: {
                            ...expression.names
                        }
                    });
                }

                const separator: string = index < keys.length - 1 ? '.' : '';
                const aliasWithoutIndexes: string = generateKeyAlias();
                let alias: string = aliasWithoutIndexes;

                key = key.replace(INDEX_PATTERN, (substring: string, index: string): string => {
                    if (firstKey && INDEX_KEY_PATTERN.test(key)) {
                        return index;
                    }

                    alias += substring;

                    return '';
                });

                return new Expression({
                    expression: expression.expression + separator + alias,
                    names: {
                        ...expression.names,
                        [aliasWithoutIndexes]: key
                    }
                });
            }, Expression.empty());
    }
}
