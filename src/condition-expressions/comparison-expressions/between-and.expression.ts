import { Expression } from '../../expression';
import { Binary } from '../../types';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * #path BETWEEN :greaterThanOrEqual AND :lessThanOrEqual
 */
export class BetweenAndExpression extends Expression {
    public constructor(path: string | Expression, greaterThanOrEqual: string | number | Binary, lessThanOrEqual: string | number | Binary) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();
        const greaterThanOrEqualAlias: string = valueAlias + 'GTOE';
        const lessThanOrEqualAlias: string = valueAlias + 'LEOE';

        super({
            expression: `${pathExpression.expression} BETWEEN ${greaterThanOrEqualAlias} AND ${lessThanOrEqualAlias}`,
            names: pathExpression.names,
            values: {
                [greaterThanOrEqualAlias]: greaterThanOrEqual,
                [lessThanOrEqualAlias]: lessThanOrEqual
            }
        });
    }
}

/**
 * @example
 * #path BETWEEN :greaterThanOrEqual AND :lessThanOrEqual
 */
export const betweenAnd: (path: string | Expression, greaterThanOrEqual: string | number | Binary, lessThanOrEqual: string | number | Binary) => BetweenAndExpression
    = (path: string | Expression, greaterThanOrEqual: string | number | Binary, lessThanOrEqual: string | number | Binary): BetweenAndExpression => new BetweenAndExpression(path, greaterThanOrEqual, lessThanOrEqual);
