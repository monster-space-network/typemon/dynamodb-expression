import { Index } from '@typemon/types';
//
import { Expression } from '../../expression';
import { Binary } from '../../types';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * #path IN (:value0, :value1, :value2, ...)
 */
export class InListExpression extends Expression {
    public constructor(path: string | Expression, value: ReadonlyArray<string | number | Binary>) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();
        const values: Index<string, string | number | Binary> = {};
        const valueExpression: string = value.slice(0, 100).map((item: string | number | Binary, index: number): string => {
            const alias: string = valueAlias + index.toString();

            values[alias] = item;

            return alias;
        }).join(', ');

        super({
            expression: `${pathExpression.expression} IN (${valueExpression})`,
            names: pathExpression.names,
            values
        });
    }
}

/**
 * @example
 * #path IN (:value0, :value1, :value2, ...)
 */
export const inList: (path: string | Expression, value: ReadonlyArray<string | number | Binary>) => InListExpression
    = (path: string | Expression, value: ReadonlyArray<string | number | Binary>): InListExpression => new InListExpression(path, value);
