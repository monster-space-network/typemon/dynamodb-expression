import { Expression } from '../../expression';
import { Value } from '../../types';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * #path < :value
 */
export class LessThanExpression extends Expression {
    public constructor(path: string | Expression, value: Value) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();

        super({
            expression: `${pathExpression.expression} < ${valueAlias}`,
            names: pathExpression.names,
            values: {
                [valueAlias]: value
            }
        });
    }
}

/**
 * @example
 * #path < :value
 */
export const lessThan: (path: string | Expression, value: Value) => LessThanExpression
    = (path: string | Expression, value: Value): LessThanExpression => new LessThanExpression(path, value);
