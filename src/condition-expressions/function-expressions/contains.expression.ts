import { Expression } from '../../expression';
import { Alias } from '../../alias';
import { Binary } from '../../types';
//
//
//
/**
 * @example
 * contains (#path, :value)
 */
export class ContainsExpression extends Expression {
    public constructor(path: string | Expression, value: string | number | Binary) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();

        super({
            expression: `contains (${pathExpression.expression}, ${valueAlias})`,
            names: pathExpression.names,
            values: {
                [valueAlias]: value
            }
        });
    }
}

/**
 * @example
 * contains (#path, :value)
 */
export const contains: (path: string | Expression, value: string | number | Binary) => ContainsExpression
    = (path: string | Expression, value: string | number | Binary): ContainsExpression => new ContainsExpression(path, value);
