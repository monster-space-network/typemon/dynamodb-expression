import { Expression } from '../../expression';
import { Alias } from '../../alias';
import { Binary } from '../../types';
//
//
//
/**
 * @example
 * begins_with (#path, :value)
 */
export class BeginsWithExpression extends Expression {
    public constructor(path: string | Expression, value: string | Binary) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();

        super({
            expression: `begins_with (${pathExpression.expression}, ${valueAlias})`,
            names: pathExpression.names,
            values: {
                [valueAlias]: value
            }
        });
    }
}

/**
 * @example
 * begins_with (#path, :value)
 */
export const beginsWith: (path: string | Expression, value: string | Binary) => BeginsWithExpression
    = (path: string | Expression, value: string | Binary): BeginsWithExpression => new BeginsWithExpression(path, value);
