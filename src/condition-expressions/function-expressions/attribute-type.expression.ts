import { Expression } from '../../expression';
import { Alias } from '../../alias';
//
//
//
export enum AttributeType {
    String = 'S',
    StringSet = 'SS',
    Number = 'N',
    NumberSet = 'NS',
    Binary = 'B',
    BinarySet = 'BS',
    Boolean = 'BOOL',
    Null = 'NULL',
    List = 'L',
    Map = 'M'
}

/**
 * @example
 * attribute_type (#path, :value)
 */
export class AttributeTypeExpression extends Expression {
    public constructor(path: string | Expression, type: AttributeType) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);
        const valueAlias: string = Alias.generateValueAlias();

        super({
            expression: `attribute_type (${pathExpression.expression}, ${valueAlias})`,
            names: pathExpression.names,
            values: {
                [valueAlias]: type
            }
        });
    }
}

/**
 * @example
 * attribute_type (#path, :value)
 */
export const attributeType: (path: string | Expression, type: AttributeType) => AttributeTypeExpression
    = (path: string | Expression, type: AttributeType): AttributeTypeExpression => new AttributeTypeExpression(path, type);
