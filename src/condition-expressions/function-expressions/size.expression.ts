import { Expression } from '../../expression';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * size (#path)
 */
export class SizeExpression extends Expression {
    public constructor(path: string | Expression) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);

        super({
            expression: `size (${pathExpression.expression})`,
            names: pathExpression.names
        });
    }
}

/**
 * @example
 * size (#path)
 */
export const size: (path: string | Expression) => SizeExpression
    = (path: string | Expression): SizeExpression => new SizeExpression(path);
