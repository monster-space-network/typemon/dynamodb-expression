import { Expression } from '../../expression';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * attribute_not_exists (#path)
 */
export class AttributeNotExistsExpression extends Expression {
    public constructor(path: string | Expression) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);

        super({
            expression: `attribute_not_exists (${pathExpression.expression})`,
            names: pathExpression.names
        });
    }
}

/**
 * @example
 * attribute_not_exists (#path)
 */
export const attributeNotExists: (path: string | Expression) => AttributeNotExistsExpression
    = (path: string | Expression): AttributeNotExistsExpression => new AttributeNotExistsExpression(path);
