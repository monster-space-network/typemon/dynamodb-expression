import { Expression } from '../../expression';
import { Alias } from '../../alias';
//
//
//
/**
 * @example
 * attribute_exists (#path)
 */
export class AttributeExistsExpression extends Expression {
    public constructor(path: string | Expression) {
        const pathExpression: Expression = Expression.isExpression(path)
            ? path
            : Alias.parsePath(path);

        super({
            expression: `attribute_exists (${pathExpression.expression})`,
            names: pathExpression.names
        });
    }
}

/**
 * @example
 * attribute_exists (#path)
 */
export const attributeExists: (path: string | Expression) => AttributeExistsExpression
    = (path: string | Expression): AttributeExistsExpression => new AttributeExistsExpression(path);
