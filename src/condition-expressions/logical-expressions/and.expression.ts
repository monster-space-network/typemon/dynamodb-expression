import { Expression } from '../../expression';
//
//
//
/**
 * @example
 * leftExpression AND rightExpression
 */
export class AndExpression extends Expression {
    public constructor(leftExpression: Expression, rightExpression: Expression) {
        super({
            expression: `${leftExpression.expression} AND ${rightExpression.expression}`,
            names: Object.assign(leftExpression.names, rightExpression.names),
            values: Object.assign(leftExpression.values, rightExpression.values)
        });
    }
}

/**
 * @example
 * leftExpression AND rightExpression
 */
export const and: (leftExpression: Expression, rightExpression: Expression) => AndExpression
    = (leftExpression: Expression, rightExpression: Expression): AndExpression => new AndExpression(leftExpression, rightExpression);
