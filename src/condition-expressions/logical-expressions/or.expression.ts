import { Expression } from '../../expression';
//
//
//
/**
 * @example
 * leftExpression OR rightExpression
 */
export class OrExpression extends Expression {
    public constructor(leftExpression: Expression, rightExpression: Expression) {
        super({
            expression: `${leftExpression.expression} OR ${rightExpression.expression}`,
            names: Object.assign(leftExpression.names, rightExpression.names),
            values: Object.assign(leftExpression.values, rightExpression.values)
        });
    }
}

/**
 * @example
 * leftExpression OR rightExpression
 */
export const or: (leftExpression: Expression, rightExpression: Expression) => OrExpression
    = (leftExpression: Expression, rightExpression: Expression): OrExpression => new OrExpression(leftExpression, rightExpression);
