import { Expression } from '../../expression';
//
//
//
/**
 * @example
 * NOT expression
 */
export class NotExpression extends Expression {
    public constructor(expression: Expression) {
        super({
            expression: `NOT ${expression.expression}`,
            names: expression.names,
            values: expression.values
        });
    }
}

/**
 * @example
 * NOT expression
 */
export const not: (expression: Expression) => NotExpression
    = (expression: Expression): NotExpression => new NotExpression(expression);
