import { Expression } from '../../expression';
//
//
//
/**
 * @example
 * (expression)
 */
export class ParenthesesExpression extends Expression {
    public constructor(expression: Expression) {
        super({
            expression: `(${expression.expression})`,
            names: expression.names,
            values: expression.values
        });
    }
}

/**
 * @example
 * (expression)
 */
export const parentheses: (expression: Expression) => ParenthesesExpression
    = (expression: Expression): ParenthesesExpression => new ParenthesesExpression(expression);
